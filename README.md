# Kettlecorn

**NOTE:** The Kettlecorn program will not directly display works restricted by Copyright by default.

A modern movie streaming client built in C++ and Qt. 

## Details

Here are some basic points about the project:
- No Electron/Chromium
- WebTorrent support
- Sleek and modern interface
- Requires no technical knowledge
- Supports a variety of APIs

Let's go into some of the specifics.

### No Electron or Chromium

Electron and Chromium for desktop apps have many practical disadvantages. 

First, they come with a lot of unneeded bloat for most programs. They often use way too much RAM than really needed.

Second, Chromium (which Electron is based upon) has nonfree blobs in all distributions of it. Which means that people who care about only using 100% free software don't like it. It also means that Electron apps cannot be admitted into many operating system distributions' software repositories.

Examples of streaming software that uses Chromium or Electron:
- Popcorn Time
- WebTorrent Desktop

### WebTorrent Support

[WebTorrent](https://webtorrent.io) is a way to use torrenting technology in the web without any browser plugins. It has massive potential, including having an app like Kettlecorn work in the web. Unfortunately, the WebTorrent protocol is slightly different from the BitTorrent protocol (to be able to make it work on the web), and thus clients have to specifically support WebTorrent to seed to peers in the web. Since we see massive potential in a potential web streaming player, we want to support seeding to WebTorrent-only capable peers as soon as possible to bridge the gap.

Examples of streaming software that supports WebTorrent and BitTorrent:
- WebTorrent Desktop

Counterexamples:
- Popcorn Time

### Sleek and Modern Interface

The software has to look good. It won't be there at first, but we want to strive to make it as stunning and easy to use as possible. We don't want Kettlecorn to seem unprofessional or subpar simply based on looks. We want people to recommend it to their friends, and to be able to use it with ease.

Examples of streaming software that has a (subjectively) modern interface:
- Popcorn Time
- WebTorrent Desktop

### Requires no technical knowledge

There are many options for torrent streaming software available, however, many of them require a bit of technical knowledge to use them effectively. We want Kettlecorn to work like a professional streaming app, requiring nothing more than searching your favorite movie and clicking play. 

Examples of streaming software that requires no technical knowledge:
- Popcorn Time

### Supports a Variety of APIs and Movies

Variety is good. To be able to compete with other streaming services, we need to provide a large selection of movies. Some streaming clients work by allowing you to search for torrent files online, but that goes against requiring no technical knowledge. Some streaming clients have a good selection of movies, but are limited to one API and requires you to search for others. Kettlecorn should support a large amount of movies (especially considering the increase of known works in the public domain), and allow the user to search for torrents easily if the user so desires. 

### Other Features

Some other features we plan to implement that will be useful to many people.
- A "Save for later" button which will download the movie to a standard location
- Seeking through a movie that's not finished downloading


Simply put, Kettlecorn should compete directly on features with Netflix and Disney Minus. We are trying to get the public to choose to use software that they control rather than software that controls them. A modern experience to watch movies in the free world is necessary.

## FAQ

Since this is just published, we don't have any frequently asked questions, but here are some answers to some potential FAQs. 

### Why C++?

[libtorrent](https://libtorrent.org/) is a torrent library that is used by many applications, such as qBittorrent. We chose it because options for torrent libraries which support WebTorrent are low. The "main" one is the WebTorrent npm module, but using JavaScript for a desktop application generally is not a great idea. There was no other option until someone started work on [adding WebTorrent support to libtorrent](https://github.com/arvidn/libtorrent/pull/4123). 

### Why use this over Popcorn Time?

Popcorn Time is great. It makes watching movies with BitTorrent easy and enjoyable for the non-techy. However, there are some downsides to it:

- It is powered by Chromium
- It doesn't support WebTorrent
- Other minor issues

## Roadmap

Here is a basic outline of the plan for Kettlecorn. It is simply an idea, and can easily be completely changed:

### Stage 1

In the first stage, we are working to support a basic foundation on which the rest of the program will be built and maintained. It will consist of mostly backend work, with a little bit of frontend to make it usable.

It will...

- Have a basic usable interface
- Implement libtorrent
- Support streaming torrents
- Have a proper build routine
- Have good code documentation

### Stage 2

In the second stage, we will work to improve the frontend to make it more usable, and start working on specific API support. We will also start making packages for GNU/Linux for people to start trying it out. 

It will...

- Have an AppImage and .deb file
- Support an API and display movies accordingly
- Finally look almost like a real streaming app
- Have a decent settings page

### Stage 3

In the third stage, we will finally be able to release a version for people to download. It will support all the basic features well, and have packages for the biggest operating systems. This will be the first stage that will have mass adoption outside of just programmers. 

It will...

- Be an official release
- Be packaged for Windows, macOS, and GNU/Linux
- Improve on stages 1 and 2
- Have a user-friendly interface
- Display a good selection of movies

### After Stage 3

After our first official release, we should have a lot of user input and ideas. The purpose of this roadmap is to jumpstart the project.

Here are some ideas:
- Improving the user interface
- Fixing bugs that were not caught earlier
- Implementing asked-for features well

## Get Involved!

So you read all this and want to contribute? Start by reading the roadmap and the issues in our bug tracker. If there is something you want to work on, comment on the relevant issue, or (if applicable) open a new one.

We are currently gathering a team and building the basic foundation for the program, join us at the [Kettlecorn Matrix room](https://matrix.to/#/#kettlecorn:matrix.org) to help out.

## License

Kettlecorn Copyright (C) 2019  [The Kettlecorn Developers](https://codeberg.org/LibreMedia/kettlecorn/src/branch/master/CREDITS)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
